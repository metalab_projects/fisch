#include <WiFi.h>
#include <ArduinoHA.h>


#define BROKER_ADDR       IPAddress(10,20,30,97)
#define WIFI_SSID         "metalab"
#define WIFI_PASSWORD     ""


int wav_0_pin = 3;
int wav_1_pin = 5;
int wav_2_pin = 7;
int wav_3_pin = 9;
int wav_4_pin = 11;
int wav_5_pin = 12;
int wav_6_pin = 1;


int en_a =4; 
int en_b =14; 


int in1 =6; 
int in2 =8; 
int in3 =10; 
int in4 =13; 


int red_button  = 18;


int led_r  = 39;
int led_g  = 35;
int led_b  = 37;


WiFiClient client;
HADevice device;

HAMqtt mqtt(client, device);




//#define LED_PIN         LED_BUILTIN



HAButton greenfish1("dingdong");
HAButton greenfish2("cleanup");
HAButton greenfish3("ruelps");
HAButton greenfish4("maunz");


//callbacks
void cb_cleanup(HAButton* sender)
{

  // digitalWrite(led_g,LOW);
  // cleanup();
  // digitalWrite(led_g,HIGH);
  
  // digitalWrite(led_r,LOW);
  // digitalWrite(led_g,LOW);
  // cleanup();
  // digitalWrite(led_g,HIGH);
  // digitalWrite(led_r,HIGH);

  digitalWrite(led_r,LOW);
  cleanup();
  digitalWrite(led_r,HIGH);
}

void cb_ruelps(HAButton* sender)
{
graus();
}

void cb_dingdong(HAButton* sender)
{
dingdong();
}

void cb_maunz(HAButton* sender)
{
maunz();
}









void setup() {




    Serial.begin(115200);
  //  pinMode(LED_PIN, OUTPUT);
  //  digitalWrite(LED_PIN, LOW);

    pinMode(wav_0_pin,OUTPUT);
    pinMode(wav_1_pin,OUTPUT);
    pinMode(wav_2_pin,OUTPUT);
    pinMode(wav_3_pin,OUTPUT);
    pinMode(wav_4_pin,OUTPUT);
    pinMode(wav_5_pin,OUTPUT);
    pinMode(wav_6_pin,OUTPUT);

    // gusch.
    digitalWrite(wav_0_pin,  HIGH);
    digitalWrite(wav_1_pin,  HIGH);
    digitalWrite(wav_2_pin,  HIGH);
    digitalWrite(wav_3_pin,  HIGH);
    digitalWrite(wav_4_pin,  HIGH);
    digitalWrite(wav_5_pin,  HIGH);
    digitalWrite(wav_6_pin,  HIGH);



    pinMode(led_r,OUTPUT);
    pinMode(led_g,OUTPUT);
    pinMode(led_b,OUTPUT);


    digitalWrite(led_r,LOW);



    //aus
    digitalWrite(led_r,HIGH);
    digitalWrite(led_g,HIGH);
    digitalWrite(led_b,HIGH);







    pinMode(en_a,OUTPUT);
    pinMode(en_b,OUTPUT);
    digitalWrite(en_a, LOW);
    digitalWrite(en_b, LOW);


    pinMode(in1,OUTPUT);
    pinMode(in2,OUTPUT);
    pinMode(in3,OUTPUT);
    pinMode(in4,OUTPUT);
    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);
    digitalWrite(in3, LOW);
    digitalWrite(in4, LOW);

    pinMode(red_button,INPUT_PULLUP);





    // set device's details (optional)
    device.setName("FISCH");
    device.setSoftwareVersion("1.0.0");

    // set icon (optional)
    greenfish1.setIcon("mdi:bell");
    greenfish1.setName("DINGDONG");
    greenfish1.onCommand(cb_dingdong);

    // // set icon (optional)
    greenfish2.setIcon("mdi:curtains");
    greenfish2.setName("CLEANUP");
    greenfish2.onCommand(cb_cleanup);


    // // set icon (optional)
     greenfish3.setIcon("mdi:air-conditioner");
     greenfish3.setName("RUELPS");
     greenfish3.onCommand(cb_ruelps);

    // // set icon (optional)
     greenfish4.setIcon("mdi:air-conditioner");
     greenfish4.setName("MAUNZ");
     greenfish4.onCommand(cb_maunz);



  // Unique ID must be set!
  byte mac[6];
  WiFi.macAddress(mac);
  device.setUniqueId(mac, sizeof(mac));
  
  
  Serial.setDebugOutput(true);
  Serial.println();



  
    digitalWrite(led_r,HIGH);



    digitalWrite(led_b,LOW);




  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  WiFi.setSleep(false);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");



  Serial.print("FISH Ready! Use 'http://");
  Serial.print(WiFi.localIP());
  Serial.println("' to connect");


    digitalWrite(led_b,HIGH);


    digitalWrite(led_g,LOW);
    
 


  mqtt.begin(BROKER_ADDR);

delay(100);
   digitalWrite(led_g,HIGH);


  
}

void loop() {
  mqtt.loop();



  if(!digitalRead(red_button))
  {
//bloedsinn();
//return;

    switch(random(0,5))
    {
    case 0:
        maunz();
        break;
    case 1:
        graus();
        break;
    case 2:
        geh_bitte();
        break;    
    case 3:
        bloedsinn();
        break;       
      case 4:
        hausverbot();
        break;    
    }



  }
}
